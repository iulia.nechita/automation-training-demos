package com.automation.training.demo;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.training.webdriver.WebDriverFactory;

public class IuliaNechitaLibris {
    @Test
    public void myLibrisTestWithWebDriver() throws Exception {
        // open the desired page
        WebDriverFactory.getDriver().get("http://www.libris.ro/");

        // set the search input locator
        By searchField = By.xpath(".//*[@id='textul']");

        By searchButtonLocator = By.xpath(".//*[@id='Body']/header/div[1]/form/input[3]");

        // identify the search input element
        WebElement search = WebDriverFactory.getDriver().findElement(searchField);

        // identify the search button element
        WebElement buttonsearch = WebDriverFactory.getDriver().findElement(searchButtonLocator);

        // type something in the search input field
        search.sendKeys("test");

        // click on the search button
        buttonsearch.click();
    }
}
