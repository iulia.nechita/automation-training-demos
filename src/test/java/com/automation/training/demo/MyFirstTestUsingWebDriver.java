package com.automation.training.demo;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.automation.training.webdriver.WebDriverFactory;

public class MyFirstTestUsingWebDriver {

    @Test
    public void myFirstTestWithWebDriver() throws Exception {
        // open the desired page
        WebDriverFactory.getDriver().get("http://dealsmaniacs.com/");

        // set the search input locator
        By searchLocator = By.cssSelector(".searchText");

        // set the search button locator
        By searchButtonLocator = By.cssSelector("#searchBox>button");

        By imagesLocator = By.cssSelector(".dealLayout");

        // identify the search input element
        // WebElement search = WebDriverFactory.getDriver().findElement(searchLocator);

        List<WebElement> listaCuImagini = WebDriverFactory.getDriver().findElements(imagesLocator);

        By priceLocator = By.cssSelector(".dealLayoutPrice>b");
        WebElement desiredCard = listaCuImagini.get(14);
        String pret = desiredCard.findElement(priceLocator).getText();

        // identify the search button element
        WebElement searchButton = WebDriverFactory.getDriver().findElement(searchButtonLocator);

        // type something in the search input field
        // search.sendKeys("this is so cool, that it works");
        // search.sendKeys(Keys.ENTER);

        // click on the search button
        searchButton.click();
        Assert.assertTrue(pret.equals("99999"));
    }
}
