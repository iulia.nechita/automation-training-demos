package com.automation.training.demo;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.automation.training.components.custom.TableHeaderComponent;
import com.automation.training.pageobjects.MenuDemoPage;
import com.automation.training.pageobjects.TableDemoPage;
import com.automation.training.webdriver.WebDriverFactory;

public class TableHeaderTest {
    MenuDemoPage menuDemo =
        PageFactory.initElements(WebDriverFactory.getDriver(), MenuDemoPage.class);

    TableDemoPage tableDemo =
        PageFactory.initElements(WebDriverFactory.getDriver(), TableDemoPage.class);

    @Test
    public void headerTest() {
        WebDriverFactory.getDriver().get(
            "https://the-internet.herokuapp.com/tables");
        WebDriverFactory.getDriver().manage().timeouts()
            .implicitlyWait(2, TimeUnit.SECONDS);

        
        tableDemo.getTableHeader().get(0).HeaderButtonLastName().hover();
        tableDemo.getTableHeader().get(1).HeaderButtonFirstName().hover();
        tableDemo.getTableHeader().get(2).HeaderButtonEmail().hover();
        tableDemo.getTableHeader().get(0).HeaderButtonLastName().click();
        tableDemo.getTableHeader().get(1).HeaderButtonFirstName().click();
        tableDemo.getTableHeader().get(2).HeaderButtonEmail().click();
        tableDemo.getTableHeader().get(3).HeaderButtonDue().click();
        tableDemo.getTableHeader().get(4).HeaderButtonWebSite().click();
        tableDemo.getTableHeader().get(5).HeaderButtonAction().click();

        System.out.println("Header contains: ");
        for (TableHeaderComponent header : tableDemo.getTableHeader()) {

            header.getText();
            System.out.println(header.getText());
        }

    }
}
