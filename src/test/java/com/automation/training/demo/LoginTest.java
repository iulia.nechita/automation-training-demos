package com.automation.training.demo;

import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import com.automation.training.components.ButtonComponent;
import com.automation.training.pageobjects.LoginPage;
import com.automation.training.pageobjects.MainPage;
import com.automation.training.pageobjects.MenuDemoPage;
import com.automation.training.pageobjects.TableDemoPage;
import com.automation.training.webdriver.WebDriverFactory;

public class LoginTest {
    MenuDemoPage menuDemo =
        PageFactory.initElements(WebDriverFactory.getDriver(), MenuDemoPage.class);

    TableDemoPage tableDemo =
        PageFactory.initElements(WebDriverFactory.getDriver(), TableDemoPage.class);

    @Test
    public void login() {
        // open the desired page
        // WebDriverFactory.getDriver().get("https://the-internet.herokuapp.com/shifting_content/menu");
        WebDriverFactory.getDriver().get("https://the-internet.herokuapp.com/tables");

        tableDemo.getTableRows().get(3).labelFirstName().getText();
        
        tableDemo.getTableRows().get(3).labelLastName().getText();
        tableDemo.getTableRows().get(0).buttonEdit().click();
        WebDriverFactory.getDriver().navigate().refresh();
        menuDemo.buttonHome().hover();
        menuDemo.buttonAbout().click();

        for (ButtonComponent button : menuDemo.getMenuButtons()) {
            button.hover();
        }

    }
    
    
  /*  @SuppressWarnings("serial")
    public class ListOfEmailMessages extends ArrayList<MessagesRowComponent> {
        public MessagesRowComponent get(String emailName) throws IndexOutOfBoundsException {
            Iterator<MessagesRowComponent> it = this.iterator();
            while (it.hasNext()) {
                MessagesRowComponent message = it.next();
                if (message.labelMessageName().getText().equals(emailName)) {
                    return message;
                }
            }
            throw new IndexOutOfBoundsException(
                String.format("Message name:'%s' was not found in the list of messages", emailName));
        }
    }*/
}
