package com.automation.training.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

/**
 * Handle the current webdriver instance.
 */
public class WebDriverFactory {

    private static WebDriver webDriverInstance = createWebDriverInstance();

    /**
     * This will return the current webdriver instance.
     * @return
     * @throws Exception
     */
    public static WebDriver getDriver() {
        if (webDriverInstance != null) {
            // maximize the window, before any action is to be done on it.
            webDriverInstance.manage().window().maximize();
            return webDriverInstance;
        } else {
            //throw new Exception("There was a problem with the current webdriver instance");
        }
        return webDriverInstance;
    }

    /**
     * Create a new firefoxDriver instance.
     * @return
     */
    private static WebDriver createWebDriverInstance() {
        return new FirefoxDriver();
        // return new ChromeDriver();
        // return new InternetExplorerDriver(caps);
        // return new SafariDriver();
    }
}
