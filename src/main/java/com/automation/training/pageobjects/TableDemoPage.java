package com.automation.training.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.automation.training.components.custom.TableHeaderComponent;
import com.automation.training.components.custom.TableRowComponent;

public class TableDemoPage {

    @FindBy(css = "#table1>tbody>tr")
    private List<WebElement> tableOneRows;

    @FindBy(css = "#table2>tbody>tr")
    private List<WebElement> tableTwoRows;

    @FindBy(css = "#table1>thead>tr>th")
    private List<WebElement> tableHeaderlist;

    public List<TableRowComponent> getTableRows() {
        List<TableRowComponent> tableRows = new ArrayList<TableRowComponent>();
        for (WebElement tableRow : tableOneRows) {
            tableRows.add(new TableRowComponent(tableRow));
        }
        return tableRows;
    }

    public List<TableRowComponent> getTableTwoRows() {
        List<TableRowComponent> tableRows = new ArrayList<TableRowComponent>();
        for (WebElement tableRow : tableTwoRows) {
            tableRows.add(new TableRowComponent(tableRow));
        }
        return tableRows;

    }

    public List<TableHeaderComponent> getTableHeader() {
        List<TableHeaderComponent> tableHeaders = new ArrayList<TableHeaderComponent>();
        for (WebElement tableHeader : tableHeaderlist) {
            tableHeaders.add(new TableHeaderComponent(tableHeader));
        }
        return tableHeaders;
    }
}
