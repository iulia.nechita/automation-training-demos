package com.automation.training.pageobjects;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.automation.training.components.ButtonComponent;
import com.automation.training.components.LabelComponent;

public class MenuDemoPage {

    @FindBy(css = "ul>li:nth-of-type(1)>a")
    private WebElement home;

    @FindBy(css = "ul>li:nth-of-type(2)>a")
    private WebElement about;

    @FindBy(css = "ul>li>a")
    private List<WebElement> menuList;

    @FindBy(css = ".example>h3")
    private WebElement labelTitleElement;

    public ButtonComponent buttonHome() {
        return new ButtonComponent(home);
    }

    public ButtonComponent buttonAbout() {
        return new ButtonComponent(about);
    }

    public LabelComponent labelTitle() {
        return new LabelComponent(labelTitleElement);
    }

    public List<ButtonComponent> getMenuButtons() {
        List<ButtonComponent> buttons = new ArrayList<ButtonComponent>();
        for (WebElement element : menuList) {
            buttons.add(new ButtonComponent(element));
        }
        return buttons;
    }
}
