package com.automation.training.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.automation.training.components.ButtonComponent;
import com.automation.training.components.InputFieldComponent;

public class LoginPage {

    @FindBy(id = "login_username")
    private WebElement userNameWebElement;

    @FindBy(id = "login_password")
    private WebElement passwdWEbElement;

    @FindBy(css = ".submitB")
    private WebElement loginWEbElement;

    public void setTextToUserName(String textToSet) {
        userNameWebElement.clear();
        userNameWebElement.sendKeys(textToSet);
    }

    public void setTextToPasswd(String passwd) {
        passwdWEbElement.clear();
        passwdWEbElement.sendKeys(passwd);
    }

    public InputFieldComponent inputPasswd() {
        return new InputFieldComponent(passwdWEbElement);
    }

    public ButtonComponent buttonLogin() {
        return new ButtonComponent(loginWEbElement) {
            @Override
            public String getText() {
                return webElement.getAttribute("title");
            }
        };
    }

    public ButtonComponent buttonDoi() {
        return new ButtonComponent(null);
    }
}
