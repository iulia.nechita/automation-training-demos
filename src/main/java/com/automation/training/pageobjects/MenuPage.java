package com.automation.training.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MenuPage {

    @FindBy(css = ".nounderline1>a")
    private WebElement loginWEbElement;

    public void clickOnLogin() {
        loginWEbElement.click();
    }
}
