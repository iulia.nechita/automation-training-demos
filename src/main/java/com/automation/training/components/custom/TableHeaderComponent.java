package com.automation.training.components.custom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.training.components.ButtonComponent;
import com.automation.training.components.Component;

public class TableHeaderComponent extends Component {
    public TableHeaderComponent(WebElement webElement) {
        super(webElement);
    }
    
    private By lastNameHeaderLocator = By.cssSelector("thead>tr>th:nth-of-type(1)>span");

    private By firstNameHeaderLocator = By.cssSelector("thead>tr>th:nth-of-type(2)>span");
    private By emailHeaderLocator = By.cssSelector("thead>tr>th:nth-of-type(3)>span");
    private By dueHeaderLocator = By.cssSelector("thead>tr>th:nth-of-type(4)>span");
    private By webSiteHeaderLocator = By.cssSelector("thead>tr>th:nth-of-type(5)>span");
    private By actionHeaderLocator = By.cssSelector("thead>tr>th:nth-of-type(6)>span");
    
    public ButtonComponent HeaderButtonLastName() {
        return new ButtonComponent(webElement.findElement(lastNameHeaderLocator));
    }

    public ButtonComponent HeaderButtonFirstName() {
        return new ButtonComponent(webElement.findElement(firstNameHeaderLocator));
    }

    public ButtonComponent HeaderButtonEmail() {
        return new ButtonComponent(webElement.findElement(emailHeaderLocator));
    }
    public ButtonComponent HeaderButtonDue() {
        return new ButtonComponent(webElement.findElement(dueHeaderLocator));
    }
    public ButtonComponent HeaderButtonWebSite() {
        return new ButtonComponent(webElement.findElement(webSiteHeaderLocator));
    }
    public ButtonComponent HeaderButtonAction() {
        return new ButtonComponent(webElement.findElement(actionHeaderLocator));
    }
}
