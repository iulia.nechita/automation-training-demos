package com.automation.training.components.custom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.automation.training.components.ButtonComponent;
import com.automation.training.components.Component;
import com.automation.training.components.LabelComponent;

public class TableRowComponent extends Component {

    public TableRowComponent(WebElement webElement) {
        super(webElement);
    }

    private By lastNameLocator = By.cssSelector("td:nth-of-type(1)");

    private By firstNameLocator = By.cssSelector("td:nth-of-type(2)");

    private By editLocator = By.cssSelector("td a:nth-of-type(1)");

    private By deleteLocator = By.cssSelector("td a:nth-of-type(2)");

    public LabelComponent labelLastName() {
        return new LabelComponent(webElement.findElement(lastNameLocator));
    }

    public LabelComponent labelFirstName() {
        return new LabelComponent(webElement.findElement(firstNameLocator));
    }

    public ButtonComponent buttonEdit() {
        return new ButtonComponent(webElement.findElement(editLocator));
    }

    public ButtonComponent buttonDelete() {
        return new ButtonComponent(webElement.findElement(deleteLocator));
    }

}
