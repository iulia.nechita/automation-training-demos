package com.automation.training.components;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class InputFieldComponent extends Component {

    public InputFieldComponent(WebElement webElement) {
        super(webElement);
    }

    public void setText(String text) {
        webElement = waitForElement(ExpectedConditions.visibilityOf(webElement));
        webElement.clear();
        webElement.sendKeys(text);
    }

    @Override
    public String getText() {
        webElement = waitForElement(ExpectedConditions.visibilityOf(webElement));
        return webElement.getAttribute("value");
    }
}
