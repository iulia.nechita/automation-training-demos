package com.automation.training.components;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.automation.training.webdriver.WebDriverFactory;

public class ButtonComponent extends Component {

    public ButtonComponent(WebElement webElement) {
        super(webElement);
    }

    public void click() {
        webElement = waitForElement(ExpectedConditions.elementToBeClickable(webElement));
        System.out.println("performing a click on: " + webElement.toString());
        webElement.click();
    }

    public void doubleClick() {
        webElement = waitForElement(ExpectedConditions.elementToBeClickable(webElement));
        System.out.println("performing a doubleClick on: " + webElement.toString());
        Actions action = new Actions(WebDriverFactory.getDriver());
        action.doubleClick(webElement).perform();
    }

    public void hover() {
        webElement = waitForElement(ExpectedConditions.elementToBeClickable(webElement));
        System.out.println("performing a hover on: " + webElement.toString());
        Actions action = new Actions(WebDriverFactory.getDriver());
        action.moveToElement(webElement).perform();
    }
}
